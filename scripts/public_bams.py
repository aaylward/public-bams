import os.path
import os
from shutil import make_archive, copy

zipdir = os.path.join(snakemake.params.output_prefix, f'kitaake-{snakemake.params.line}')
os.mkdir(zipdir)
for bam in snakemake.input.bams:
    copy(bam, os.path.join(zipdir, os.path.basename(bam)))
for bai in snakemake.input.bais:
    copy(bai, os.path.join(zipdir, os.path.basename(bai)))
make_archive(zipdir, 'zip', base_dir=zipdir)
