# Imports ======================================================================
from snakemake.utils import validate

# Config =======================================================================

validate(config, "config/config.schema.json")

target_zip = config["output_prefix"] + "/kitaake-" + config["line"] + ".zip"

# Rules ========================================================================

# rule all:
#     input:
#         target_zip

rule public_bams:
    input:
        bams = [config["input_prefix"] + "/" + config["line"] + "_" + s + ".bam" for s in config["samples"]],
        bais = [config["input_prefix"] + "/" + config["line"] + "_" + s + ".bam.bai" for s in config["samples"]]
    output:
        z = target_zip
    conda:
        "config/requirements.yml"
    params:
        output_prefix = config["output_prefix"],
        line = config["line"]
    resources:
        disk_mb = 1_000_000
    script:
        "scripts/public_bams.py"
